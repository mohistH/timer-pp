#include <iostream>
#include <ITimerPP.h>
#include <memory>
#include <thread>

using namespace oct_tk;

#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/timeb.h> 


/// ----------------------------------------------------------------------------------------
/// 年月日时分秒毫秒
struct st_now_date_
{
    unsigned short	year_;
    unsigned char	month_;
    unsigned char	day_;

    unsigned char	hour_;
    unsigned char	minute_;
    unsigned char	second_;
    unsigned short	mill_sec_;		/// 毫秒

    void zero_()
    {
        //memset(this, 0, sizeof(st_now_date_));
        year_ = 0;
        month_ = 0;
        day_ = 0;

        hour_ = 0;
        minute_ = 0;
        second_ = 0;
        mill_sec_ = 0;
    }

    st_now_date_()
    {
        zero_();
    }
};
typedef st_now_date_ st_now_date;

/// --------------------------------------------------------------------------------------------------------
///	@brief: 返回年月日时分秒毫秒
/// --------------------------------------------------------------------------------------------------------
void get_date_now_()
{

    st_now_date date;

    time_t tt = time(nullptr);
    struct tm *stm = localtime(&tt);

    date.year_ = stm->tm_year + 1900;
    date.month_ = stm->tm_mon + 1;
    date.day_ = stm->tm_mday;

    date.hour_ = stm->tm_hour;
    date.minute_ = stm->tm_min;
    date.second_ = stm->tm_sec;

    /// 得到毫秒
    struct timeb tb;
    ftime(&tb);
    date.mill_sec_ = tb.millitm;

    std::cout << date.mill_sec_ << "\n";

    // return date;
}

class demoTimerCallback : public oct_tk::ITimerCallBack
{
public:
    virtual void timer_call_back_()
    {
        static int index = 0; 
        std::cout << "\n index=" << ++index << "=";
        get_date_now_();
    }
};




int main(int argc, char* argv[], char* env[])
{
    demoTimerCallback tc;

    std::unique_ptr<ITimerPP> demo_timer = oct_tk::new_itimer_();

    ITimerPP* ptimer = demo_timer.get();

    ptimer->init_(oct_tk::ITimerPP::TIMER_TYPE_PERIODIC, &tc);
    ptimer->begin_(40);


    std::this_thread::sleep_for(std::chrono::seconds(1 * 30));
    ptimer->end_();

    return 0;
}
#include "ITimerPP.h"
#include "TimerPPImp.h"

namespace oct_tk
{
	/// --------------------------------------------------------------------------------
	/// @brief: oct_tk::new_itimer_
	/// --------------------------------------------------------------------------------
	//ITimer* oct_tk::new_itimer_()
	//{
	//	ITimer* pobj = new(std::nothrow) TimerImp;

	//	return pobj;
	//}

	/// --------------------------------------------------------------------------------
	/// @brief: new_itimer_
	/// --------------------------------------------------------------------------------
	std::unique_ptr<oct_tk::ITimerPP> new_itimer_()
	{
		return std::make_unique<oct_tk::TimerPPImp>();
	}

	/// --------------------------------------------------------------------------------
	/// @brief: del_itimer_
	/// --------------------------------------------------------------------------------
	//ITimer* del_itimer_(ITimer* pobj)
	//{
	//	if (pobj)
	//	{
	//		pobj->end_();
	//		delete pobj;
	//		pobj = nullptr;
	//	}

	//	return pobj;
	//}

}
#ifndef ITIMER_H_
#define ITIMER_H_

#include <memory>


	/// 定义导出符
	/// ---------------------------------------------------------------------------------------
#if defined(_WIN32) || defined(_WIN64) || defined(WIN32)
	/// ---------------------------------------------------------------------------------------------
	#ifndef LIB_OCT_TK_API
		#define LIB_OCT_TK_API	__declspec(dllexport)
	#else
		#define LIB_OCT_TK_API	__declspec(dllimport)
	#endif /// !lib_toolkits_api

#elif defined(_unix_) || defined(_linux_) || defined(_unix) || defined(_linux) || defined(__unix)  || defined (_apple_)
	/// ---------------------------------------------------------------------------------------------
	#ifndef LIB_OCT_TK_API
		#define LIB_OCT_TK_API	__attribute__((visibility ("default")))
	#endif /// !lib_toolkits_api

#endif /// !defined(_WIN32) || defined(_WIN64) || defined(WIN32)



namespace oct_tk
{

	/// ----------------------------------------------------------------------------------------
	/// @brief: 定时器回调执行函数
	/// ----------------------------------------------------------------------------------------
	class ITimerCallBack
	{
	public:
		virtual ~ITimerCallBack() {}

		virtual void timer_call_back_() = 0;
	};



	/// ----------------------------------------------------------------------------------------
	/// @brief: 定时器接口
	/// ----------------------------------------------------------------------------------------
	class ITimerPP
	{

	public:

		using uint = unsigned int;

		enum TimerType
		{
			/// 单次执行
			TIMER_TYPE_ONE_SHORT	= 0,
			/// 周期执行
			TIMER_TYPE_PERIODIC		= 1,
		};

	public:
		virtual ~ITimerPP() {  }

		virtual int init_(const TimerType&& tt, ITimerCallBack* pcb) = 0;

		virtual int begin_(const uint&& interval_ms) = 0;

		virtual int end_() = 0;
	};



	LIB_OCT_TK_API std::unique_ptr<ITimerPP>	new_itimer_();

	//LIB_OCT_TK_API ITimer* new_itimer_();


	//LIB_OCT_TK_API ITimer* del_itimer_(ITimer* pobj);



}



#endif /// ITIMER_H_
#ifndef XTIMER_H_
#define XTIMER_H_
	/// 定义导出符
	/// ---------------------------------------------------------------------------------------
#if defined(_WIN32) || defined(_WIN64) || defined(WIN32)
	/// ---------------------------------------------------------------------------------------------
	#ifndef OS_IS_WIN
		#define OS_IS_WIN	
	#endif /// !OS_IS_WIN

#elif defined(_unix_) || defined(_linux_) || defined(_unix) || defined(_linux) || defined(__unix)  || defined (_apple_)
	/// ---------------------------------------------------------------------------------------------
	#ifndef OS_IS_LNIX
		#define OS_IS_LNIX	
	#endif /// !OS_IS_LNIX

#endif /// !defined(_WIN32) || defined(_WIN64) || defined(WIN32)


#include "ITimerPP.h"

#if defined (OS_IS_WIN)
#include <wtypes.h>
#elif defined (OS_IS_LNIX)
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <iostream>


#endif /// 




namespace oct_tk
{

	/// ----------------------------------------------------------------------------------------
	/// @brief: 定时器
	/// ----------------------------------------------------------------------------------------
	class TimerPPImp : public ITimerPP
	{
	public:
		TimerPPImp();
		virtual ~TimerPPImp();

		TimerPPImp(const TimerPPImp& instance) = delete;
		TimerPPImp(const TimerPPImp&& instance) = delete;
		TimerPPImp& operator = (const TimerPPImp& instance) = delete;
		TimerPPImp& operator = (const TimerPPImp&& instance) = delete;


		/// --------------------------------------------------------------------------------
		/// @brief: init_
		/// --------------------------------------------------------------------------------
		virtual int init_(const TimerType&& tt, ITimerCallBack* pcb) override;


		/// --------------------------------------------------------------------------------
		/// @brief: begin_
		/// --------------------------------------------------------------------------------
		virtual int begin_(const uint&& interval_ms) override;


		/// --------------------------------------------------------------------------------
		/// @brief: end_
		/// --------------------------------------------------------------------------------
		virtual int end_() override;


	protected:
		/// 定时器回调函数
		ITimerCallBack* pcallback_func_ = nullptr;

		/// 定时器类型
		TimerType		timer_type_ = ITimerPP::TIMER_TYPE_ONE_SHORT;
#ifdef OS_IS_WIN
		static void CALLBACK TimeProc(UINT uTimerID, UINT uMsg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2);

		/// 定时器ID
		UINT			timer_id_ = 0;
#elif  defined (OS_IS_LNIX)
		timer_t timerid = nullptr;
		struct sigevent sev;
		struct sigaction sa;
		struct itimerspec its;

		inline static void handler(int, siginfo_t* si, void*)
		{
			int index = 0;
			// std::cout << "\nAAAAAAAA=====index=" << ++index ;

			ITimerCallBack* pcb = (reinterpret_cast<TimerPPImp*>(si->si_value.sival_ptr))->pcallback_func_;
			if (pcb)
			{
				pcb->timer_call_back_();

				// std::cout << "\n2222333333=====index=" << ++index ;
			}
		}
#endif /// 
	};

}



#endif /// XTIMER_H_